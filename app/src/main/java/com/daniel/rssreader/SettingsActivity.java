package com.daniel.rssreader;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;

public class SettingsActivity extends AppCompatActivity {

    private CheckBox checkBoxHideImages;
    private Spinner spinnerFontPicker;
    private ArrayAdapter<String> adapter;
    private final String[] fontArray = new String[]{"Roboto", "Roboto Condensed"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle(R.string.settings_title);

        checkBoxHideImages = findViewById(R.id.checkBoxDisplayImages);
        spinnerFontPicker = findViewById(R.id.spinnerFontPicker);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, fontArray);
        spinnerFontPicker.setAdapter(adapter);

        checkBoxHideImages.setChecked(!GlobalData.isHideImages());
        spinnerFontPicker.setSelection(Arrays.asList(fontArray).indexOf(GlobalData.getFont()));

        checkBoxHideImages.setOnClickListener(view -> GlobalData.setHideImages(!checkBoxHideImages.isChecked()));

        spinnerFontPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GlobalData.setFont(fontArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }
}