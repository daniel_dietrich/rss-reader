package com.daniel.rssreader;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

// Author: Daniel Dietrich
public class MainActivity extends AppCompatActivity {

    public final static List<SyndFeed> FEED_LIST = new ArrayList<>();

    private final SyndFeedInput syndFeedInput = new SyndFeedInput();
    private final List<String> feedTitleList = new ArrayList<>();

    private FloatingActionButton addFeedButton;
    private ListView listViewFeeds;

    private ArrayAdapter arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle(getResources().getString(R.string.main_activity_title));
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());

        addFeedButton = findViewById(R.id.addFeedButton);
        listViewFeeds = findViewById(R.id.listViewFeeds);

        registerForContextMenu(listViewFeeds);

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, feedTitleList);
        listViewFeeds.setAdapter(arrayAdapter);

        listViewFeeds.setOnItemClickListener((adapterView, view, i, l) -> {
            GlobalData.setFeedIndex(i);

            Intent intent = new Intent(this, FeedActivity.class);
            startActivity(intent);
        });

        addFeedButton.setOnClickListener(view -> showInputDialog());
    }

    protected void showInputDialog() {
        final float DPI = getResources().getDisplayMetrics().density;
        final EditText editTextAddress = new EditText(this);

        editTextAddress.setText("https://blog.pragmaticengineer.com/rss/");
        editTextAddress.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);

        AlertDialog dialog = (new AlertDialog.Builder(this))
                .setTitle(R.string.new_feed_title)
                .setPositiveButton(R.string.add_button, (dialogInterface, i) -> {
                    String newFeedAddress = editTextAddress.getText().toString();

                    try {
                        URL feedSource = new URL(newFeedAddress);
                        SyndFeed feed = syndFeedInput.build(new XmlReader(feedSource));

                        if (!FEED_LIST.contains(feed)) {
                            FEED_LIST.add(feed);
                            feedTitleList.add(feed.getTitle());
                            listViewFeeds.invalidateViews();
                        } else {
                            Toast.makeText(this, getResources().getString(R.string.feed_exists_error), Toast.LENGTH_SHORT).show();
                        }

                    } catch (FeedException | IOException e) {
                        Toast.makeText(this, getResources().getString(R.string.add_feed_error), Toast.LENGTH_SHORT).show();
                    }

                })
                .setNegativeButton(R.string.cancel_button, null)
                .create();

        dialog.setView(editTextAddress, (int) (10 * DPI), (int) (5 * DPI), (int) (10 * DPI), (int) (5 * DPI));
        dialog.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listViewFeeds) {
            menu.add("Delete");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals("Delete")) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Toast.makeText(this, feedTitleList.get(info.position) + R.string.remove_feed, Toast.LENGTH_SHORT).show();

            // NOT SAFE: Program assumes the same position of title and feed itself in their own lists
            feedTitleList.remove(info.position);
            FEED_LIST.remove(info.position);
            listViewFeeds.invalidateViews();
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu1, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemSettings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;

            case R.id.itemAbout:
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("About RSS Reader");
                alertDialog.setMessage("RSS Reader was built by Daniel Dietrich");
                alertDialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveStringToPreferences(String str){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("label", str);
        editor.apply();
    }
}

